cls:	
	pushw %bp
	movw %sp, %bp

	# Limpa tela
	movb $0x06, %ah # scroll up
	movb $0x00, %al #  line to scroll, 0 to clear
	movb $0x00, %cl # left column number
	movb $0x00, %ch # upper row number
	movb $0x4f, %dl # right column number 
	movb $0x18, %dh # lower column number
	movb $0x02, %bh # background color 
	int $0x10
	
	# Volta cursor para posição inicial
	movb $0x00, %bh # page number
	movb $0x00, %dh # row
	movb $0x00, %dl # column
	movb $0x02, %ah # set cursor position
	int $0x10

	leave
	ret

print:
	pushw %bp
	movw %sp, %bp

	# Posicionar cursor
	movb $0x02, %ah # set cursor position
	movb 6(%bp), %dh # row
	movb 8(%bp), %dl # column
	int $0x10
	
	movw 4(%bp), %si
	jmp print_cont
	
print_char:
	# Imprimir na tela
	movb $0x0e, %ah # teletype
	movb $0x00, %bh # page number
	movb $0x02, %bl # color
	int $0x10
	
	inc %si
	
print_cont:	
	movb (%si), %al
	cmpb $0, %al
	jne print_char

	leave
	ret
