	.code16
	.text
	.globl _start
	
_start:
	# Seleciona a primeira tela
	movb $0x00, %al # page number
	movb $0x05, %ah # set active display
	int $0x10
	
	call cls	

	pushw $0x840
	pushw $0x00
	pushw $0x15
	pushw $0x00
	pushw $0x02
	pushw $0x00
	pushw $0x00
	call read_sector

	call load_gdt

	call protected_mode

	cli

	movw $0x10, %ax
	movw %ax, %ds

	xchg %bx, %bx
	jmp $0x08,$0x8400

# escrita na memória
read_sector:
	pushw %bp
	movw %sp, %bp
	
	movb $0x02, %ah # read sector

	movb 12(%bp), %al
	movb 10(%bp), %ch
	movb 8(%bp), %cl
	movb 6(%bp), %dh
	movb 4(%bp), %dl

	movw 16(%bp), %bx
	movw %bx, %es
	movw 14(%bp), %bx

	int $0x13
	
	leave
	ret

load_gdt:
	push %bp
	movw %sp, %bp
	
	lgdt gdt_ptr
	
	leave
	ret

gdt_ptr:
	.word gdt_end - gdt
	.int gdt


gdt:
	.byte 0, 0, 0, 0, 0, 0, 0, 0	      # null segment
	.byte 0xff, 0xff, 0, 0, 0, 0x9A, 0xCF, 0 # code segment
	.byte 0xff, 0xff, 0, 0, 0, 0x92, 0xCF, 0 # data segment
gdt_end:	

protected_mode:
	push %bp
	movw %sp, %bp

	movl %cr0, %eax
	or $1, %eax
	movl %eax, %cr0
	
	leave
	ret

	
bootloader:
	.asciz "Meu Bootloader" #linha 1, coluna 1

carregando:
	.asciz "Carregando kernel" #linha 2, coluna 1

loading_gdt:
	.asciz "Carregando GDT"

loaded_gdt:
	.asciz "GDT Carregada"

switched:
	.asciz "Modo protegido ativado"
	
	.include "src/bootloader/function.s"

	
	. = _start + 510
	.byte 0x55
	.byte 0xaa

