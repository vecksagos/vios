#include "global.h"
#include "isr.h"
#include "idt.h"


void *isr_routines[32] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

extern void isr0();
extern void isr1();
extern void isr2();
extern void isr3();
extern void isr4();
extern void isr5();
extern void isr6();
extern void isr7();
extern void isr8();
extern void isr9();
extern void isr10();
extern void isr11();
extern void isr12();
extern void isr13();
extern void isr14();
extern void isr15();
extern void isr16();
extern void isr17();
extern void isr18();
extern void isr19();
extern void isr20();
extern void isr21();
extern void isr22();
extern void isr23();
extern void isr24();
extern void isr25();
extern void isr26();
extern void isr27();
extern void isr28();
extern void isr29();
extern void isr30();
extern void isr31();
extern void isr32();

enum exception {
  DIVISION_BY_ZERO = 0,
  DEBUG,
  NON_MASKABLE_INTERRUPT,
  BREAKPOINT,
  INTO_DETECTED_OVERFLOW,
  OUT_OF_BOUND,
  INVALID_OPCODE,
  NO_COPROCESSOR,
  DOUBLE_FAULT,
  COPROCESSOR_SEGMENT_OVERRUN,
  BAD_TSS,
  SEGMENT_NOT_PRESENT,
  STACK_FAULT,
  GENERAL_PROTECTOIN_FAULT,
  PAGE_FAULT,
  UNKNOWN_INTERRUPT,
  COPROCESSOR_FAULT,
  ALIGNMENT_CHECK,
  MACHINE_CHECK,
  RESERVED19,
  RESERVED20,
  RESERVED21,
  RESERVED22,
  RESERVED23,
  RESERVED24,
  RESERVED25,
  RESERVED26,
  RESERVED27,
  RESERVED28,
  RESERVED29,
  RESERVED30,
  RESERVED31
};

const char *exception_messages[] =
  {
    "Division By Zero",
    "Debug",
    "Non Maskable Interrupt",
    "Breakpoint",
    "Into Detected Overflow",
    "Out of Bounds",
    "Invalid Opcode",
    "No Coprocessor",
    "Double Fault",
    "Coprocessor Segment",
    "Bad TSS",
    "Segment Not Present",
    "Stack Fault",
    "General Protection",
    "Page Fault",
    "Unknown Interrupt",
    "Coprocessor Fault",
    "Alignment Check",
    "Machine Check",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved"
  };

void isr_install_handler(int isr, void (*handler)(struct regs *r)) {
    isr_routines[isr] = handler;
}

void isr_uninstall_handler(int isr) {
  isr_routines[isr] = 0;
}

void isr_init() {
  idt_set_gate(DIVISION_BY_ZERO, (unsigned)isr0, 0x08, 0x8E);
  idt_set_gate(DEBUG, (unsigned)isr1, 0x08, 0x8E);
  idt_set_gate(NON_MASKABLE_INTERRUPT, (unsigned)isr2, 0x08, 0x8E);
  idt_set_gate(BREAKPOINT, (unsigned)isr3, 0x08, 0x8E);
  idt_set_gate(INTO_DETECTED_OVERFLOW, (unsigned)isr4, 0x08, 0x8E);
  idt_set_gate(OUT_OF_BOUND, (unsigned)isr5, 0x08, 0x8E);
  idt_set_gate(INVALID_OPCODE, (unsigned)isr6, 0x08, 0x8E);
  idt_set_gate(NO_COPROCESSOR, (unsigned)isr7, 0x08, 0x8E);
  idt_set_gate(DOUBLE_FAULT, (unsigned)isr8, 0x08, 0x8E);
  idt_set_gate(COPROCESSOR_SEGMENT_OVERRUN, (unsigned)isr9, 0x08, 0x8E);
  idt_set_gate(BAD_TSS, (unsigned)isr10, 0x08, 0x8E);
  idt_set_gate(SEGMENT_NOT_PRESENT, (unsigned)isr11, 0x08, 0x8E);
  idt_set_gate(STACK_FAULT, (unsigned)isr12, 0x08, 0x8E);
  idt_set_gate(GENERAL_PROTECTOIN_FAULT, (unsigned)isr13, 0x08, 0x8E);
  idt_set_gate(PAGE_FAULT, (unsigned)isr14, 0x08, 0x8E);
  idt_set_gate(UNKNOWN_INTERRUPT, (unsigned)isr15, 0x08, 0x8E);
  idt_set_gate(COPROCESSOR_FAULT, (unsigned)isr16, 0x08, 0x8E);
  idt_set_gate(ALIGNMENT_CHECK, (unsigned)isr17, 0x08, 0x8E);
  idt_set_gate(MACHINE_CHECK, (unsigned)isr18, 0x08, 0x8E);
  idt_set_gate(RESERVED19, (unsigned)isr19, 0x08, 0x8E);
  idt_set_gate(RESERVED20, (unsigned)isr20, 0x08, 0x8E);
  idt_set_gate(RESERVED21, (unsigned)isr21, 0x08, 0x8E);
  idt_set_gate(RESERVED22, (unsigned)isr22, 0x08, 0x8E);
  idt_set_gate(RESERVED23, (unsigned)isr23, 0x08, 0x8E);
  idt_set_gate(RESERVED24, (unsigned)isr24, 0x08, 0x8E);
  idt_set_gate(RESERVED25, (unsigned)isr25, 0x08, 0x8E);
  idt_set_gate(RESERVED26, (unsigned)isr26, 0x08, 0x8E);
  idt_set_gate(RESERVED27, (unsigned)isr27, 0x08, 0x8E);
  idt_set_gate(RESERVED28, (unsigned)isr28, 0x08, 0x8E);
  idt_set_gate(RESERVED29, (unsigned)isr29, 0x08, 0x8E);
  idt_set_gate(RESERVED30, (unsigned)isr30, 0x08, 0x8E);
  idt_set_gate(RESERVED31, (unsigned)isr31, 0x08, 0x8E);
}

void fault_handler(struct regs *r)  {
    void (*handler)(struct regs *r);
    handler = isr_routines[r->int_no];
    if (handler)
        handler(r);
    
    if (r->int_no >= 40)
        outportb(0xA0, 0x20);
    
    outportb(0x20, 0x20);
}
