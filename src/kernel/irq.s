.macro irqi n
	.globl irq\n
irq\n:
	cli
	push $0x0
	push $\n
	jmp irq_common_stub
.endm
	
.extern irq_handler

irqi 0
irqi 1
irqi 2
irqi 3
irqi 4
irqi 5
irqi 6
irqi 7
irqi 8
irqi 9
irqi 10
irqi 11
irqi 12
irqi 13
irqi 14
irqi 15
	
irq_common_stub:
	pusha
	push %ds
	push %es
	push %fs
	push %gs
	mov $0x10, %ax
	mov %ax, %ds
	mov %ax, %es
	mov %ax, %fs
	mov %ax, %gs
	mov %esp, %eax
	push %eax
	call irq_handler
	pop %eax
	pop %gs
	pop %fs
	pop %es
	pop %ds
	popa
	add $8, %esp
	iret
