#include "gdt.h"

extern void gdt_load(struct gdtPtr*);

void gdt_init() {
  gdtPtr.limit = (sizeof(struct gdtSeg) * GDTSIZE) - 1;
  gdtPtr.base = (uint32_t) gdt;

  gdt_add_seg(0, 0x0, 0x0, 0x0, 0x0); // null
  gdt_add_seg(1, 0x0, 0xffffffff, 0x0, 0xa); // type = 0xa = code
  gdt_add_seg(2, 0x0, 0xffffffff, 0x0, 0x2); //  type = 0x2 = data

  gdt_load(&gdtPtr);
}

void gdt_add_seg(uint32_t ind, uint32_t base,
		 uint32_t size, uint8_t ring, uint8_t type) {
  if (ind == 0) {
    gdt[0].base_low = 0;
    gdt[0].base_middle = 0;
    gdt[0].base_high = 0;
    gdt[0].limit_low = 0;
    gdt[0].granularity = 0;
    gdt[0].access = 0;

    return;
  }

  gdt[ind].base_low    = (base & 0xffff);       // low = 16 primeiros
						// bits de base
  gdt[ind].base_middle = ((base >> 16) & 0xff); // high =  8 ultimos
  gdt[ind].base_high   = ((base >> 24) & 0xff); // middle = q sobrou

  gdt[ind].limit_low   = (size & 0xffff);       // low = 16 primeiros
  gdt[ind].granularity = ((size >> 16) & 0x0f); // gran = 0b1100 +
						// size 4 primeiros
  gdt[ind].granularity |= 0xc0;

  gdt[ind].access = (type & 0xf);              // access = 0b1 + 2
                                               // bits do ring + 0b1
                                               // + 4 bits do type
  ring = ring << 8;
  gdt[ind].access |= ring;
  gdt[ind].access |= 0x90;
}
