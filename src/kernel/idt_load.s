	.globl idt_load

idt_load:
	push %ebp
	movl %esp, %ebp

	movl 8(%ebp), %eax
	lidt (%eax)
	
	leave
	ret
