#include "global.h"
#include "paging.h"
#include "isr.h"
#include "video.h"

void paging_init() {
    struct pageDir *dir;
    struct pageTable *table0;

    dir = (struct pageDir*) 0x10000;
    table0 = (struct pageTable*) 0x11000;

    int i;
    for (i = 0; i < PAGINGSIZE; ++i) {
        table0->pages[i].present    = 1;
        table0->pages[i].read_write = 1;
        table0->pages[i].user       = 1;
        table0->pages[i].write      = 0;
        table0->pages[i].frame_base = i;
    }



    uint32_t *tmp = (uint32_t*) (((uint32_t) table0) | 0x07);

    isr_install_handler(14, page_fault);

    dir->tables[0] = (struct pageTable*) tmp;

    struct pageTable *table1 = (struct pageTable*) 0x12000;

    for (i = 0; i < PAGINGSIZE; ++i) {
        table1->pages[i].present    = 0;
        table1->pages[i].read_write = 1;
        table1->pages[i].user       = 1;
        table1->pages[i].write      = 0;
        table1->pages[i].frame_base = i + 2048;

        if (i % 2)
           table1->pages[i].present = 1;
    }

    uint32_t *tmp1 = (uint32_t*) (((uint32_t) table1) | 0x7);

    dir->tables[1] = (struct pageTable*) tmp1;

    update_directory(dir);
    enable_paging();
}

void page_fault(struct regs *r) {
    uint32_t fault_address;
    asm volatile("mov %%cr2, %0" : "=r" (fault_address));

    int present = !(r->err_code & 0x1);
    int rw = r->err_code & 0x2;
    int us = r->err_code & 0x4;
    int reserved = r->err_code & 0x8;

    if (fault_address / ((4*PAGINGSIZE*PAGINGSIZE)) > 2) {
        puts("\n");
        puts("permission error");
    } else if (present) {

        uint32_t tables = fault_address / 4*PAGINGSIZE;
        uint32_t t_index = tables / PAGINGSIZE;
        uint32_t p_index = tables - (PAGINGSIZE * t_index);

        struct pageTable *table1 = (struct pageTable*) 0x12000;
        table1->pages[p_index].present = 1;

        write_page((uint8_t*) fault_address, (uint8_t*) "UFAL#");

        puts("\n");
        puts("Page fault! ( ");
        if (present)
            puts("present ");
        if (rw)
            puts("read-only ");
        if (us)
            puts("user-mode ");
        if (reserved)
            puts("reserved");

    while(1);
    }
}


void write_page(uint8_t *address, uint8_t *texto) {
    uint32_t i, j = 0;
    for (i = 0; i < 4*PAGINGSIZE; i+= j)
        for (j = 0; j < sizeof(texto); ++j)
            address[i+j] = texto[j];
}
