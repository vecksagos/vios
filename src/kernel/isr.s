.macro isri n
	.globl isr\n
isr\n:
	cli
	push  $0x0
	push $\n
	jmp isr_common_stub
.endm

.macro isrc n
	.globl isr\n
isr\n:
	cli
	push $\n
	jmp isr_common_stub
.endm
	
.extern fault_handler
	
isri 0
isri 1
isri 2
isri 3
isri 4
isri 5
isri 6
isri 7
isrc 8
isri 9
isrc 10
isrc 11
isrc 12
isrc 13
isrc 14
isri 15
isri 16
isri 17
isri 18
isri 19
isri 20
isri 21
isri 22
isri 23
isri 24
isri 25
isri 26
isri 27
isri 28
isri 29
isri 30
isri 31
isri 32
	
isr_common_stub:	
	pusha
	push %ds
	push %es
	push %fs
	push %gs
	mov $0x10, %ax
	mov %ax, %ds
	mov %ax, %es
	mov %ax, %fs
	mov %ax, %gs
	mov %esp, %eax
	push %eax
	call fault_handler
	 
	pop %eax
	pop %gs
	pop %fs
	pop %es
	pop %ds
	popa
	add $8, %esp
	iret
