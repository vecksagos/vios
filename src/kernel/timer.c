#include "global.h"
#include "irq.h"
#include "video.h"

int timer_ticks = 0;

void timer_handler(struct regs *r) {
  ++timer_ticks;
  if (!(timer_ticks % 18))
    puts("One second has passed\n");
}

void timer_init() {
    irq_install_handler(0, timer_handler);
}

void timer_wait(int ticks) {
  uint64_t eticks;
  eticks = timer_ticks + ticks;
  while(timer_ticks < eticks);
}

void timer_phase(int hz)
{
    int divisor = 1193180 / hz;       /* Calculate our divisor */
    outportb(0x43, 0x36);             /* Set our command byte 0x36 */
    outportb(0x40, divisor & 0xFF);   /* Set low byte of divisor */
    outportb(0x40, divisor >> 8);     /* Set high byte of divisor */
}
