        .globl enable_paging
        .globl update_directory

enable_paging:
        push %ebp
        mov %esp, %ebp

        xchg %bx, %bx
        mov %cr0, %eax
        or $0x80000000, %eax
        mov %eax, %cr0

        leave
        ret

update_directory:
        push %ebp
        mov %esp, %ebp

        xchg %bx, %bx
        mov 8(%ebp), %eax
        mov %eax, %cr3

        leave
        ret
