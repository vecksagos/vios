	.globl gdt_load
	
gdt_load:
	push %ebp
	movl %esp, %ebp

	movl 8(%ebp), %eax

	
	lgdt (%eax)

	mov $0x10, %ax
	mov %ax, %ds
	mov %ax, %ss
	mov %ax, %fs
	mov %ax, %gs
	mov %ax, %es
	
	leave
	ret
