#include "global.h"
#include "gdt.h"
#include "idt.h"
#include "keyboard.h"
#include "timer.h"
#include "video.h"
#include "isr.h"
#include "irq.h"
#include "paging.h"

int main() {

  gdt_init();
  idt_init();
  isr_init();
  irq_init();
  //timer_init();
  keyboard_init();
  video_init();
  paging_init();

  asm("sti");

  uint8_t *page11 = (uint8_t*) 0x40b000;
  write_page(page11, (uint8_t*) "IC#");
  uint8_t *teste = (uint8_t*) 0x400000;
  teste[0] = "a";
  while(1);

  return 0;
}

