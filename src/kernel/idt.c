#include "idt.h"

struct idtPtr {
  uint16_t limit;
  uint32_t base;
} __attribute__((packed));

extern void idt_load(struct idtPtr*);

void idt_init() {

  struct idtPtr idtPtr;
  idtPtr.limit = (sizeof(struct idtEntry) * IDTSIZE) - 1;
  idtPtr.base = (uint32_t) idt;

  memset((uint8_t*) idt, 0, (uint32_t) (sizeof(struct idtEntry) * IDTSIZE));

  idt_load(&idtPtr);
}

void idt_set_gate(uint32_t ind, uint32_t base, uint16_t sel, uint8_t flags) {
    idt[ind].base_low = (base & 0xFFFF);
    idt[ind].base_high = (base >> 16) & 0xFFFF;
    idt[ind].sel = sel;
    idt[ind].always0 = 0;
    idt[ind].flag = flags;
}
