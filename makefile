all: bootloader.img

auxiliar:
	mkdir -p bin
	mkdir -p target

gdt_load.o:
	as src/kernel/gdt_load.s -o target/gdt_load.o

idt_load.o:
	as src/kernel/idt_load.s -o target/idt_load.o

isra.o:
	as src/kernel/isr.s -o target/isra.o

irqa.o:
	as src/kernel/irq.s -o target/irqa.o

bootloader.o:
	as src/bootloader/bootloader.s -o target/bootloader.o

paginga.o:
	as src/kernel/paging.s -o target/paginga.o

gdt.o:
	gcc -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o target/gdt.o src/kernel/gdt.c

idt.o:
	gcc -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o target/idt.o src/kernel/idt.c

global.o:
	gcc -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o target/global.o src/kernel/global.c

isr.o:
	gcc -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o target/isr.o src/kernel/isr.c

irq.o:
	gcc -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o target/irq.o src/kernel/irq.c

timer.o:
	gcc -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o target/timer.o src/kernel/timer.c

keyboard.o:
	gcc -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o target/keyboard.o src/kernel/keyboard.c

video.o:
	gcc -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o target/video.o src/kernel/video.c

paging.o:
	gcc -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o target/paging.o src/kernel/paging.c

kernel.o: gdt.o idt.o global.o isr.o irq.o timer.o keyboard.o video.o paging.o
	gcc -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o target/kernel.o src/kernel/kernel.c

kernel.bin : kernel.o gdt_load.o idt_load.o isra.o irqa.o paginga.o
	ld -Ttext 0x8400 --oformat=binary target/kernel.o  target/gdt.o target/gdt_load.o target/idt.o target/idt_load.o target/isra.o target/irqa.o target/irq.o target/isr.o target/global.o target/keyboard.o target/timer.o target/video.o target/paging.o target/paginga.o -o bin/kernel.bin

bootloader.bin: bootloader.o
	ld -Ttext 0x7c00 --oformat=binary target/bootloader.o -o bin/bootloader.bin

bootloader.img: auxiliar bootloader.bin	kernel.bin
	cat bin/bootloader.bin bin/kernel.bin > bin/floppy.bin
	@dd if=bin/floppy.bin of=fd0

clean:
	rm -Rf bin/ target/ floppy.bin none fd0
