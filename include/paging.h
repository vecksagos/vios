#ifndef PAGING_
#define PAGING_

#define PAGINGSIZE 1024

struct regs;

struct pageEntry {
    uint8_t present     :1;
    uint8_t read_write  :1;
    uint8_t user        :1;
    uint8_t write       :1;
    uint8_t cache       :1;
    uint8_t accessed    :1;
    uint8_t size        :1;
    uint8_t ignored     :1;
    uint8_t available   :4;
    uint32_t frame_base :20;
};

struct pageTable {
    struct pageEntry pages[PAGINGSIZE];
};

struct pageDir {
    struct pageTable *tables[PAGINGSIZE];
};

extern void enable_paging();
extern void update_directory(struct pageDir *);

void paging_init();
void page_fault(struct regs *r);
void write_page(uint8_t *end, uint8_t *texto);

#endif
