#ifndef VIDEO_
#define VIDEO_

void puts(char *text);
void putch(char c);
void scroll(void);
void move_csr(void);
void settextcolor(uint8_t forecolor, uint8_t backcolor);
void video_init();

#endif
