#ifndef GLOBAL_
#define GLOBAL_

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long uint64_t;

struct regs
{
    uint32_t gs, fs, es, ds;      /* pushed the segs last */
    uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax;  /* pushed by 'pusha' */
    uint32_t int_no, err_code;    /* our 'push byte #' and ecodes do this */
    uint32_t eip, cs, eflags, useresp, ss;   /* pushed by the processor automatically */ 
};

void *memcpy(void *dest, const void *src, uint32_t count);
void memset(uint8_t *dest, uint8_t val, uint32_t len);
uint16_t *memsetw(uint16_t *dest, uint16_t val, uint32_t count);
uint32_t strlen(const char *str);
uint8_t inportb (uint16_t _port);
void outportb (uint16_t _port, uint8_t _data);

#endif
