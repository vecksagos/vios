#ifndef ISR_
#define ISR_

struct idtEntry;
struct regs;

void isr_init();
void isr_install_handler(int isr, void (*handler)(struct regs *r));
void isr_uninstall_handler(int isr);
void fault_handler(struct regs *r);

#endif
