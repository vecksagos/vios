#ifndef GDT_
#define GDT_

#include "global.h"

#define GDTSIZE 3

struct gdtSeg {
  uint16_t limit_low;
  uint16_t base_low;
  uint8_t base_middle;
  uint8_t access;
  uint8_t granularity;
  uint8_t base_high;
} __attribute__((packed));

struct gdtPtr {
  uint16_t limit;
  uint32_t base;
} __attribute__((packed));

struct gdtSeg gdt[GDTSIZE];

struct gdtPtr gdtPtr;
void gdt_init();
void gdt_add_seg(uint32_t ind, uint32_t base,
		 uint32_t size, uint8_t ring, uint8_t type);
#endif
