#ifndef IDT_
#define IDT_

#include "global.h"

struct idtEntry {
  uint16_t base_low;
  uint16_t sel;
  uint8_t always0;
  uint8_t flag;
  uint16_t base_high;
} __attribute__((packed));

#define IDTSIZE 256

struct idtEntry idt[IDTSIZE];

void idt_init();
void idt_set_gate(uint32_t ind, uint32_t base, uint16_t sel, uint8_t flags);

#endif
