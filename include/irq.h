#ifndef IRQ_
#define IRQ_
struct idtEntry;

void irq_init();
void irq_install_handler(int irq, void (*handler)(struct regs *r));
void irq_uninstall_handler(int irq);

#endif
